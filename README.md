# [`dune-extensions`](https://gitlab.com/dune-archiso/repository/dune-extensions) repository for Arch Linux

[![pipeline status](https://gitlab.com/dune-archiso/repository/dune-extensions/badges/main/pipeline.svg)](https://gitlab.com/dune-archiso/repository/dune-extensions/-/commits/main)
[![coverage report](https://gitlab.com/dune-archiso/repository/dune-extensions/badges/main/coverage.svg)](https://gitlab.com/dune-archiso/repository/dune-extensions/-/commits/main)

This is a third-party auto-updated repository with the following [tarballs](https://gitlab.com/dune-archiso/repository/dune-extensions/-/raw/main/packages.x86_64). The DUNE core modules are here.

## Usage

### Add repository

1. Import GPG key from GPG servers.

```console
[user@hostname ~]$ sudo pacman-key --recv-keys 2403871B121BD8BB
[user@hostname ~]$ sudo pacman-key --finger 2403871B121BD8BB
[user@hostname ~]$ sudo pacman-key --lsign-key 2403871B121BD8BB
```

2. Add the following code to `/etc/pacman.conf`:

```toml
[dune-extensions]
SigLevel = Required DatabaseOptional
Server = https://dune-archiso.gitlab.io/repository/dune-extensions/$arch
```

### List packages

To show actual packages list:

```console
[user@hostname ~]$ pacman -Sl dune-extensions
```

### Install packages

To install package:

```console
[user@hostname ~]$ pacman -Syy
[user@hostname ~]$ pacman -S dune-foamgrid dune-spgrid
```

<!-- - [dune-alugrid](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-alugrid/README.md)
- [dune-codegen](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-codegen/README.md)
- [dune-foamgrid](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-foamgrid/README.md)
- [dune-grid-glue](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-grid-glue/README.md)
- [dune-metagrid](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-metagrid/README.md)
- [dune-multidomaingrid](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-multidomaingrid/README.md)
- [dune-polygongrid](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-polygongrid/README.md)
- [dune-prismgrid](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-prismgrid/README.md)
- [dune-spgrid](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-spgrid/README.md)
- [dune-tpmc](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-tpmc/README.md)
- [dune-vcfv](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-vcfv/README.md)
- [dune-vectorclass](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-vectorclass/README.md)
- [dune-vtk](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-vtk/README.md) -->
